 Online Human-Assisted Learning using Random Ferns

  Description:
    This file contains the code to perform online and human-assisted learning
    using random ferns [1][2]. More specifically, this code allows to learn
    and detect simultaneously one specific object using human assistance.
    Initially, the human selects via the computer's mouse the object in the
    image that he/she wants to learn and recognize in future frames.
    Subsequently, the random ferns classifier is initially computed using
    a set of training samples. Positive samples are extracted using random
    shift transformations over the object, whereas negative samples are random
    patches from the background.

    In run time, the random ferns detect the object and use their own hypotheses
    to update and refine the classifier (self-learning). However, in cases where
    the classifier is uncertain about its output (sample label), the classifier
    requests the human assistance in order to label the difficult samples. In this
    case, the program asks to the human user if the object detection (bounding box)
    is correct or not. The user should answer yes (y) or not (n) via the keyboard.

    If you make use of this code for research articles, we kindly encourage
    to cite the references [1][2], listed below. This code is only for research
    and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4. ./detector (using webcam)
 
  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions: fun_show_frame and
       fun_update_classifiers are shown properly. We recommend not using a
       resolution lower of 640x480 pixels.
    2. The program parameters are defined in the file parameters.txt located
       in the files folder. In this file you can change the detector parameters
       and program funcionalities.

  Keyboard commands:
    p: pause the video stream
    ESC: exit the program
    space: enable/disable object detection
    y: yes
    n: no

  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Online Human-Assisted Learning using Random Ferns
        M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
        International Conference on Pattern Recognition (ICPR), 2012.

    [2] Proactive Behavior of an Autonomous Mobile Robot for Human-Assisted Learning
        A. Garrell, M. Villamizar, F. Moreno-Noguer and A. Sanfeliu
        International Symposium on Robot and Human Interactive Communication (IEEE RO-MAN), 2013.


