#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "detector_classes.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// save image
void fun_save_image(cv::Mat img, long int number){

  // num. max. images
  int numMaxImgs = 10000;

  // variables
  char imgName[50];

  // path and image name
  if (number<10000)
    sprintf(imgName, "../images/image_%ld.jpg",number);
  if (number<1000)
    sprintf(imgName, "../images/image_0%ld.jpg",number);
  if (number<100)
    sprintf(imgName, "../images/image_00%ld.jpg",number);
  if (number<10)
    sprintf(imgName, "../images/image_000%ld.jpg",number);

  // save image
  if (number<numMaxImgs)
    cv::imwrite(imgName, img);

};

// draw message
void fun_draw_message(clsParameters* prms, cv::Mat img, char* text, CvPoint location, CvScalar textColor){

  // text font
  float font = prms->fun_get_font();

  // message
  cv::putText(img, text, location, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);

};

// draw rectangle
void fun_draw_rectangle(clsParameters* prms, cv::Mat img, CvRect rec, CvScalar recColor){

  // rectangle thickness
  int recThick = prms->fun_get_rectangle_thickness();

  // top-left and bottom-right points
  CvPoint topLeft = cvPoint(rec.x, rec.y);
  CvPoint bottomRight = cvPoint(rec.x+rec.width, rec.y+rec.height);

  // draw rectangle
  cv::rectangle(img, topLeft, bottomRight, recColor, recThick);

};

// show frame
void fun_show_frame(clsParameters* prms, cv::Mat img, int contFrame, double FPS){

  // text font
  float font = prms->fun_get_font();

  // rectangle color and thickness
  CvScalar textColor = prms->fun_get_color();

  // rectangle coordinates
  CvPoint xy1 = cvPoint(2,30);
  CvPoint xy2 = cvPoint(250,30);

  // text variables
  char text1[50];
  char text2[50];

  // messages
  sprintf(text1, "#%i",contFrame);
  sprintf(text2, "FPS: %.1f",FPS);
  cv::putText(img, text1, xy1, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);
  cv::putText(img, text2, xy2, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);

};

// show image patch
void fun_show_image_patch(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet){

  // patch size
  int patchSize = prms->fun_get_patch_size();

  // max. detection
  clsDetection* maxDet = new clsDetection;

  // visualization area (top-right corner)
  CvRect area = cvRect(img.cols-patchSize, 1, patchSize, patchSize);

  // maxima detection
  detSet->fun_get_max_detection(maxDet);

  // detection variables
  float score;  // detection score
  int x1,y1,x2,y2;  // detection location

  // detection values
  maxDet->fun_get_values(x1, y1, x2, y2, score);

  // bounding box
  CvRect rec = cvRect(x1, y1, x2-x1, y2-y1);

  // check
  if (x1!=0 && x2!=0 && y1!=0 && y2!=0){

    // patch
    cv::Mat patch = cv::Mat(cvSize(patchSize,patchSize), CV_8UC3);
    cv::resize(img(rec), patch, cvSize(patchSize,patchSize));

    // add patch into image
    addWeighted(img(area), 0.1, patch, 0.9, 0.0, img(area));
  }

  // release
  delete maxDet;
};

// draw detection rectangle
void fun_draw_detection(cv::Mat img, clsDetection* det, CvScalar recColor, int recThick, float font){

  // variables
  float score;  // detection score
  char text[50];  // score text
  int x1,x2,y1,y2;  // detection location

  // detection values
  det->fun_get_values(x1, y1, x2, y2, score);

  // rectangle coordinates
  CvPoint topLeft = cvPoint(x1, y1);  // top left coordinate
  CvPoint bottomRight = cvPoint(x2, y2);  // bottom right coordinate

  // draw rectangle
  cv::rectangle(img, topLeft, bottomRight, recColor, recThick);

  // draw detection score
  sprintf(text, "%.2f",score);
  cv::putText(img, text, topLeft, cv::FONT_HERSHEY_PLAIN, font, recColor, 4);
};

// draw detections
void fun_draw_detections(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels){

  // rectangle color and thickness
  CvScalar recColor = prms->fun_get_color();
  int recThick = prms->fun_get_rectangle_thickness();

  // text font
  float font = prms->fun_get_font();

  // number of detections
  int numDets = detSet->fun_get_num_detections();

  // pointer to labels
  float* labPtr = labels.ptr<float>(0);

  // draw each detection
  for (int iter=0; iter<numDets; iter++){

    // sample label
    float label = (float)*(labPtr + iter);

    // only positive detections are shown on the screen

    // positive sample
    if (label==1.0){

      // detection color (green)
      recColor = cvScalar(0, 255, 0);

      // current detection
      clsDetection* det = detSet->fun_get_detection(iter);

      // draw detection
      fun_draw_detection(img, det, recColor, recThick, font);
    }
  }
};

// image equalization
void fun_image_equalization(cv::Mat img){

  // temporal image
  cv::Mat img_space = img;
  cv::resize(img_space, img_space, cvSize(img.cols, img.rows));

  // convert color space: YCrCb or Lab
  cvtColor(img,img_space,CV_BGR2Lab);

  // split image into color channels
  std::vector<cv::Mat> img_channels;
  cv::split(img_space, img_channels);

  // equalize histograms
  cv::equalizeHist(img_channels[0], img_channels[0]);  // equalize channel 1

  // merge channels
  cv::merge(img_channels, img_space);

  // convert color space: YCrCb or Lab
  cvtColor(img_space, img, CV_Lab2BGR);

};

// detection labels
cv::Mat fun_detection_labels(clsParameters* prms, clsDetectionSet* detSet){

  // detection and assistance thresholds
  float detThr = prms->fun_get_threshold();
  float omega = prms->fun_get_assistance_threshold();

  // number of detections
  int numDets = detSet->fun_get_num_detections();

  // create detection labels
  cv::Mat labels = cv::Mat(numDets, 1, CV_32FC1, cv::Scalar::all(0));

  // pointer to labels
  float* labPtr = labels.ptr<float>(0);

  // detection variables
  float score;  // detection score
  int xa,ya,xb,yb;  // detection location

  // detections
  for (int iter=0; iter<numDets; iter++){

    // current detection
    clsDetection* det = detSet->fun_get_detection(iter);

    // detection values
    det->fun_get_values(xa, ya, xb, yb, score);

    // positive detection
    if (score>detThr + 0.5*omega){*(labPtr + iter) = 1.0;}

    // negative detection
    if (score<detThr - 0.5*omega){*(labPtr + iter) = -1.0;}

    // human asssitance -uncertainty region-
    if ((score >= detThr - 0.5*omega) && (score <= detThr + 0.5*omega)){*(labPtr + iter) = 0.5;}

  }

  // return detection labels
  return labels;
};

// fern maps
cv::Mat fun_fern_maps(cv::Mat img, clsRFs* RFs){

  // detector parameters
  int width = img.cols;  // image width
  int height = img.rows;  // image height
  int numChannels = img.channels();  // num. image channels
  int numFerns = RFs->fun_get_num_ferns();  // number of ferns
  int fernSize = RFs->fun_get_fern_size();  // fern size
  int numFeatures = RFs->fun_get_num_features();  // number of features per fern
  cv::Mat fernsData = RFs->fun_get_random_ferns();  // pointer to random ferns

  // create ferns maps
  cv::Mat fernsMaps = cv::Mat(cvSize(width,height), CV_16UC(RFs->fun_get_num_ferns()));

  // pointers to ferns and image data
  unsigned char *imgPtr = (unsigned char*)(img.data);
  unsigned char *fernsPtr = (unsigned char*)(fernsData.data);
  unsigned short *mapsPtr = (unsigned short*)(fernsMaps.data);

  // variables
  int tmp = numFeatures*6;

  // scanning
  for (int y=0; y<height-fernSize; y++) {
    for (int x=0; x<width-fernSize; x++) {
      for (int fern=0; fern<numFerns; fern++){

      // fern index
      int z = 0;

      // fern features
      for (int feature=0; feature<numFeatures; feature++){

        // point A coordinates
        int ya = y + (int)*(fernsPtr + fern*tmp + feature*6 + 0);
        int xa = x + (int)*(fernsPtr + fern*tmp + feature*6 + 1);
        int ca =     (int)*(fernsPtr + fern*tmp + feature*6 + 2);

        // image value A
        float va = *(imgPtr + ya*width*numChannels + xa*numChannels + ca);

        // point B coordinates
        int yb = y + (int)*(fernsPtr + fern*tmp + feature*6 + 3);
        int xb = x + (int)*(fernsPtr + fern*tmp + feature*6 + 4);
        int cb =     (int)*(fernsPtr + fern*tmp + feature*6 + 5);

        // image value B
        float vb = *(imgPtr + yb*width*numChannels + xb*numChannels + cb);

        // value comparison
        z += (1 << feature) & (0 - (va > vb));

      }
      // fern output
      *(mapsPtr + y*width*numFerns + x*numFerns + fern) = z;
      }
    }
  }

  // return fern maps
  return fernsMaps;
};

// scanning window
void fun_scanning_window(cv::Mat fernsMaps, clsClassifier* clfr, clsDetectionSet* detSet){

  // parameters
  int minWCs = 50;  // num. min. tested weak classifiers
  bool speed = true;  // naive cascade
  float detThr = 0.3;  // default detector threshold
  float minThr = 0.3;  // min. threshold (naive cascade)

  // variables
  int detCont = 0;  // detection counter
  int objHeight,objWidth;  // object size

  // detector parameters
  cv::Mat WCs = clfr->fun_get_WCs();  // pointer to classifier data
  int numWCs = clfr->fun_get_num_WCs();  // num. weak classifiers
  cv::Mat ratHstms = clfr->fun_get_ratHstms();  // classifier distributions
  int numBins = ratHstms.cols;  // num. histogram bins

  // object size
  clfr->fun_get_object_size(objHeight, objWidth);

  // number muximum of detections
  int numMaxDets = detSet->fun_get_num_max_detections();

  // ferm map size
  int width = fernsMaps.cols;
  int height = fernsMaps.rows;
  int numFerns = fernsMaps.channels();

  // pointer to fern maps, weak classifiers and fern ratio distributions
  float* ratPtr = ratHstms.ptr<float>(0);
  unsigned char *WCsPtr = (unsigned char*)(WCs.data);
  unsigned short *mapsPtr = (unsigned short*)(fernsMaps.data);

  // scanning
  for (int y=0; y<height-objHeight; y++){
    for (int x=0; x<width-objWidth; x++){

      // detection score
      float score = 0;

      // weak classifiers
      for (int WC=0; WC<numWCs; WC++){

        // weak classifier values
        int py = y + (int)*(WCsPtr + WC*3 + 0);
        int px = x + (int)*(WCsPtr + WC*3 + 1);
        int pf =     (int)*(WCsPtr + WC*3 + 2);

        // fern output
        int z = *(mapsPtr + py*width*numFerns + px*numFerns + pf);

        // update detection score
        score+= *(ratPtr + WC*numBins + z);

        // a naive cascade to speed up detection
        if (speed==true && (WC+1)>minWCs && (score/(WC+1))<minThr) break;
      }

      // normalize score
      score = score/numWCs;

      // check amount of detections
      if (detCont>numMaxDets)
        printf("!! Warning: there are many hypotheses: low threshold\n");

      // save detections
      if(score>detThr && detCont<numMaxDets){

        // temporal detection
        clsDetection* det = new clsDetection;

        // set detection values
        det->fun_set_values(x+1, y+1, x+objWidth, y+objHeight, score);

        // add detection to detection set
        detSet->fun_set_detection(det, detCont);

        // increment the number of detections
        detCont++;

        // save the number of detections
        detSet->fun_set_num_detections(detCont);

        // release
        delete det;
      }
    }
  }
};

// object detection
void fun_detect(clsParameters* prms, clsClassifier* clfr, clsRFs* RFs, cv::Mat img, clsDetectionSet* detSet){

  // parameters
  int minCell = prms->fun_get_min_cell_size();  // min. cell size
  int maxCell = prms->fun_get_max_cell_size();  // min. cell size
  int numFerns = RFs->fun_get_num_ferns();  // num. random ferns

  // variables
  clsII II;  // integral image
  int width,height;  // image size

  // number muximum of output detections
  int numMaxDets = detSet->fun_get_num_max_detections();

  // compute integral image
  II.fun_integral_image(img);

  // image levels -scales-
  for (int cellSize=minCell; cellSize<maxCell; cellSize++){

    // temporal detections
    clsDetectionSet* dets = new clsDetectionSet;

    // compute image from II
    II.fun_compute_image(cellSize);
    II.fun_get_image_size(width, height);

    // ferns maps -convolve ferns with the image-
    cv::Mat fernsMaps = fun_fern_maps(II.fun_get_image(), RFs);

    // test the classifier
    fun_scanning_window(fernsMaps, clfr, dets);

    // non maxima supression
    dets->fun_non_maxima_supression();

    // scaling detection coordinates
    dets->fun_scaling(cellSize);

    // adding detections
    detSet->fun_add_detections(dets);

    // release image
    II.fun_release_image();

    // release
    delete dets;
  }

  // non maxima supression
  detSet->fun_non_maxima_supression();
};

// update classifier using positive smaples
void fun_update_positive_samples(cv::Mat img, CvRect rec, clsClassifier* clfr, clsRFs* RFs, int numSamples, float shift){

  // parameters
  int minSize = 5;  // min. image size
  float posLabel = 1.0;  // sample label

  // object size
  int height,width;
  clfr->fun_get_object_size(height, width);

  // positive samples
  for (int iter=0; iter<numSamples; iter++){

    // new rectangle coordinates
    int ya = rec.y + round(shift*rec.height*((float)rand()/RAND_MAX - 0.5));
    int xa = rec.x + round(shift*rec.width*((float)rand()/RAND_MAX  - 0.5));
    int yb = rec.y + rec.height + round(shift*rec.height*((float)rand()/RAND_MAX - 0.5));
    int xb = rec.x + rec.width + round(shift*rec.width*((float)rand()/RAND_MAX  - 0.5));

    // check
    if ((xb-xa)>minSize && (yb-ya)>minSize){

      // check limits
      if (xa<0){xa = 0;}
      if (ya<0){ya = 0;}
      if (xb>=img.cols){xb = img.cols-1;}
      if (yb>=img.rows){yb = img.rows-1;}

      // new bounding box
      CvRect newRec = cvRect(xa, ya, xb-xa, yb-ya);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(width,height), CV_8UC3);
      cv::resize(img(newRec), patch, cvSize(width,height));

      // ferns maps -convolve ferns with the image-
      cv::Mat fernsMaps = fun_fern_maps(patch, RFs);

      // update classifier
      clfr->fun_update(fernsMaps, posLabel);

    }
  }
};

//update classifier using negative samples
void fun_update_negative_samples(cv::Mat img, CvRect rec, clsClassifier* clfr, clsRFs* RFs, int numSamples, float shift){

  // parameters
  int minSize = 5;  // min. image size
  float negLabel = -1.0;  // sample label

  // object size
  int height,width;
  clfr->fun_get_object_size(height, width);

  // positive samples
  for (int iter=0; iter<numSamples; iter++){

    // new rectangle coordinates (random location)
    int xi = round(img.cols*((float)rand()/RAND_MAX));
    int xj = round(img.cols*((float)rand()/RAND_MAX));
    int yi = round(img.rows*((float)rand()/RAND_MAX));
    int yj = round(img.rows*((float)rand()/RAND_MAX));
    int xa = min(xi, xj);
    int xb = max(xi, xj);
    int ya = min(yi, yj);
    int yb = max(yi, yj);

    // check
    if ((xb-xa)>minSize && (yb-ya)>minSize){

      // check limits
      if (xa<0){xa = 0;}
      if (ya<0){ya = 0;}
      if (xb>=img.cols){xb = img.cols-1;}
      if (yb>=img.rows){yb = img.rows-1;}

      // new bounding box
      CvRect newRec = cvRect(xa, ya, xb-xa, yb-ya);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(width,height), CV_8UC3);
      cv::resize(img(newRec), patch, cvSize(width,height));

      // ferns maps -convolve ferns with the image-
      cv::Mat fernsMaps = fun_fern_maps(patch, RFs);

      // update classifier
      clfr->fun_update(fernsMaps, negLabel);

    }
  }
};

// update classifier
void fun_update_classifier(clsParameters* prms, clsClassifier* clfr, clsRFs* RFs, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels){

  // parameters
  float shift  = prms->fun_get_image_shift();  // image shift factor
  int recThick = prms->fun_get_rectangle_thickness();  // detection rectangle thickness
  int numSamples = prms->fun_get_num_new_samples();  // number of new -extracted- samples

  // variables
  CvScalar color;  // color
  float score;  // detection score
  cv::Mat copImg;   // copy image
  int x1,y1,x2,y2; // detection location
  int height,width;  // object size

  // positive and negative values
  float posValue = 1.0;
  float negValue = -1.0;

  // text font
  float font = prms->fun_get_font();

  // number of detections
  int numDets = detSet->fun_get_num_detections();

  // image copy: for learning without messages
  img.copyTo(copImg);

  // check
  if ((numDets!=0) && (numSamples>0)){

    // pointer to labels
    float* labPtr = labels.ptr<float>(0);

    // detections
    for (int iter=0; iter<numDets; iter++){

      // current detection
      clsDetection* det = detSet->fun_get_detection(iter);

      // detection values
      det->fun_get_values(x1, y1, x2, y2, score);

      // object size
      clfr->fun_get_object_size(height, width);

      // new bounding box
      CvRect rec = cvRect(x1, y1, x2-x1, y2-y1);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(width,height), CV_8UC3);
      cv::resize(img(rec), patch, cvSize(width,height));

      // ferns maps -convolve ferns with the image-
      cv::Mat fernsMaps = fun_fern_maps(patch, RFs);

      // sample label
      float label = (float)*(labPtr + iter);

      // human assistance
      if (label==0.5){

        // put message
        cv::putText(img, "Is it correct?", cvPoint(10, 30), cv::FONT_HERSHEY_PLAIN, font, cvScalar(0, 255, 255), 4);

        // detection rectangle
        cv::rectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), cvScalar(0, 255, 255), recThick);
        cv::imshow("detection", img);

        // user answer
        char key = cvWaitKey();
        switch(key){
          case 'y':
            // set color
            color = cvScalar(0, 255, 0);

            // correct label
            *(labPtr + iter) = 1;

            // update positive sample
            clfr->fun_update(fernsMaps, posValue);

            // update classifier using a set of positive samples
            fun_update_positive_samples(copImg, rec, clfr, RFs, numSamples, shift);
            break;

          case 'n':
            // set color
            color = cvScalar(0, 0, 255);

            // correct label
            *(labPtr + iter) = -1;

            // update pnegative sample
            clfr->fun_update(fernsMaps, negValue);

            // update classifier using a set of negative samples
            fun_update_negative_samples(copImg, rec, clfr, RFs, numSamples, shift);
            break;
        }

        // detection rectangle
        cv::rectangle(img, cvPoint(x1, y1), cvPoint(x2, y2), color, recThick);
        cv::imshow("detection", img);
        cvWaitKey(33);

      }

      // update positive sample
      if (label==1.0){
        // update positive sample
        clfr->fun_update(fernsMaps, posValue);
        // update using a set of positive samples
        fun_update_positive_samples(copImg, rec, clfr, RFs, numSamples, shift);
      }

      // update negative sample
      if (label==-1.0){
        // update negative sample
        clfr->fun_update(fernsMaps, negValue);
        // update using a set of negative samples
        fun_update_negative_samples(copImg, rec, clfr, RFs, numSamples, shift);
      }

    }
  }
};

// train classifier
void fun_train_classifier(clsParameters* prms, clsClassifier* clfr, clsRFs* RFs, cv::Mat img, CvRect rec){

  // parameters
  float shift = prms->fun_get_image_shift();  // image shift factor
  int numTrnSamples = prms->fun_get_num_train_samples();  // number of training samples

  // object size
  int height = prms->fun_get_object_height();
  float ratio = (float)rec.width/rec.height;
  int width = (int)round(ratio*height);

  // set the object size
  clfr->fun_set_object_size(height, width);

  // initialize the classifier using the current object size
  clfr->fun_set_threshold(prms->fun_get_threshold());
  clfr->fun_compute(prms->fun_get_num_WCs(), RFs->fun_get_num_ferns(), RFs->fun_get_num_features(), RFs->fun_get_fern_size());
  clfr->fun_print_classifier();

  // update with positive samples
  fun_update_positive_samples(img, rec, clfr, RFs, numTrnSamples, shift);

  // update with negative samples
  fun_update_negative_samples(img, rec, clfr, RFs, numTrnSamples, shift);

};



