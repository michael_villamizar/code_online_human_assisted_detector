#ifndef objectDetector_h
#define objectDetector_h

#include <list>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// program parameters
class clsParameters{
  private:
    int imgWidth;  // image width
    int imgHeight;  // image height
    int objWidth;  // object width
    int objHeight;  // object height
    int numFerns;  // num. random ferns
    int numFeatures;  // num. features per fern
    int fernSize;  // fern size
    int numWCs;  // num. weak classifiers
    float threshold;  // classifier threshold
    float omega;  // assistance threshold
    int recThick;  // rectangle thickness
    int numTrnSamples;  // num. training samples
    int numNewSamples;  // num. new (bootstrapped) samples
    float imgShift;  // image shift factor
    int minCellSize;  // min. cell size
    int maxCellSize;  // max. cell size
    CvScalar color;  // default color
    float font;  // text font
    int patchSize;  // patch size
    int imgEqualization;  // image equalization
    int saveImages;  // save images
    char const* filePath;  // file parameters path
  public:
    clsParameters();  // constructor
    ~clsParameters();  // destructor
    void fun_initialize();  // initialize variables
    void fun_load_parameters();  // load parameters from file
    int fun_get_image_width();  // return image width
    int fun_get_image_height();  // return image height
    int fun_get_object_width();  // object width
    int fun_get_object_height();  // object height
    int fun_get_num_ferns();  // return num. random ferns
    int fun_get_num_features();  // return num. features
    int fun_get_fern_size();  // return fern size
    int fun_get_num_WCs();  // return the number of weak classifiers
    float fun_get_threshold();  // return the classifier threshold
    float fun_get_image_shift();  // return the image shift rate
    int fun_get_num_train_samples();  // return the number of training samples
    int fun_get_num_new_samples();  // return the number of new bootstrapped samples
    void fun_print_parameters();  // print program parameters
    int fun_get_min_cell_size();  // get min. cell size
    int fun_get_max_cell_size();  // get max. cell size
    int fun_get_patch_size();  // patch size
    float fun_get_font();  // return the text font
    int fun_get_rectangle_thickness();  // return rectangle thickness
    int fun_image_equalization();  // image equalization
    int fun_save_images();  // save images
    CvScalar fun_get_color();  // return rectangle color
    float fun_get_assistance_threshold();  // return the assistance threshold (omega)
};

// random ferns -RFs-
class clsRFs {
  private:
    int numFerns;  // num. shared random ferns
    int numFeatures;  // num. binary features per fern
    int fernSize;  // spatial fern size
    int fernDepth;  // fern depth
    cv::Mat data;  // random ferns data
  public:
    clsRFs();  // constructor
    ~clsRFs();  // destructor
    void fun_print_ferns();  // print fern parameters
    int fun_get_num_ferns();  // return the number of random ferns
    int fun_get_fern_size();  // return the fern size
    int fun_get_num_features();  // return the number of features per fern
    cv::Mat fun_get_random_ferns();  // get random ferns
    void fun_random_ferns(int ferns, int features, int size);  // create random ferns
};

// classifier
class clsClassifier {
  private:
    int numWCs;  // number of weak classifiers
    int numBins;  // number of histogram bins
    int objWidth;  // object image width
    int objHeight;  // object image height
    float threshold;  // threshold
    cv::Mat WCs;  // weak classifiers
    cv::Mat posHstms;  // positive fern histograms
    cv::Mat negHstms;  // negative fern histograms
    cv::Mat ratHstms;  // ratio of fern histograms
  public:
    clsClassifier();  // constructor
    ~clsClassifier();  // destructor
    int fun_get_num_WCs();  // get the number of weak classifiers
    cv::Mat fun_get_WCs();  // pointer to weak classifier data
    cv::Mat fun_get_posHstms();  // pointer to positive fern distributions
    cv::Mat fun_get_negHstms();  // pointer to negative fern distributions
    cv::Mat fun_get_ratHstms();  // pointer to ratio of fern distributions
    float fun_get_threshold();  // return classifier threshold
    void fun_print_classifier();  // print classifier parameters
    void fun_set_threshold(float thr);  // set detector threshold
    void fun_update(cv::Mat &fernMaps, float label);  // update the object classifier
    void fun_set_object_size(int height, int width);  // set object size
    void fun_get_object_size(int& height, int& width);  // return object size
    void fun_compute(int numWCs, int numFerns, int numFeatures, int fernSize);  // compute the object classifier
};

// one instance detection
class clsDetection {
  private:
    float score;  // detection score
    int x1,y1,x2,y2;  // detection location
  public:
    clsDetection();  // constructor
    ~clsDetection();  // destructor
    void fun_initialize();  // initialize
    void fun_set_values(int x1, int y1, int x2, int y2, float score);  // set detection values
    void fun_get_values(int &x1, int &y1, int &x2, int &y2, float &score);  // return detection values
};

// detection set -multiple detections-
class clsDetectionSet {
  private:
    int numDetections;  // number of detections
    int numMaxDetections;  // number muximum of detections
    clsDetection* detection;  // array of detections
  public:
    clsDetectionSet();  // constructor
    ~clsDetectionSet();  // destructor
    void fun_release();  // release memory
    void fun_initialize();  // initialize
    int fun_get_num_detections();  // return number of detections
    int fun_get_num_max_detections();  // return number muximum of detections
    void fun_get_max_detection(clsDetection* maxDet);  // return the maximun detection
    clsDetection* fun_get_detection(int index);  // return indexed detection
    void fun_non_maxima_supression();  // return maxima detections
    void fun_set_num_detections(int value);  // set number of detections
    void fun_scaling( float scaleFactor );  // scale detection coordinates
    void fun_set_detection(clsDetection* det, int index);  // set indexed detection
    void fun_add_detections(clsDetectionSet* detections);  // add new detections
    void fun_remove_detections(clsDetection* detection);  // remove detections using overlapping measure
};

  // integral image
class clsII {
  private:
    cv::Mat II;  // integral image
    cv::Mat img;  // image data
    int width;  // integral image width
    int height;  // integral image height
    int cellSize;  // cell size -pixelsxpixels-
    int imgWidth;  // image width
    int imgHeight;  // image height
    int numChannels;  // number of image channels : i.e color
  public:
    clsII();  // constructor
    ~clsII();  // destructor
    cv::Mat fun_get_image();  // return the image pointer
    void fun_release_image();  // release image
    void fun_compute_image(int size);  // compute image from II
    void fun_integral_image(cv::Mat img);  // compute integral image
    void fun_get_image_size(int& sx, int& sy);  // return image size
};

#endif


